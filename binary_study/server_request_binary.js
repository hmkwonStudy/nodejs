const http = require('http');
const url = require('url');
const fs = require('fs');
const mime = require('mime');

const server = http.createServer((request, response) => {
   const parsedUrl = url.parse(request.url);
   const resource = parsedUrl.pathname;

   if(resource.indexOf('/images/') === 0){
      const imgPath = resource.substring(1);
      console.log('imgPath=' + imgPath);

      const imgMime = mime.getType(imgPath);
      console.log('mime' + imgMime);

      fs.readFile(imgPath, (err, data) => {
         if(err){
            response.writeHead(500, {'Content-Type' : 'text/html'});
            response.end('500 Internal Server ' + err);
         } else {
            response.writeHead(200, {'Content-Type' : imgMime});
            response.end(data);
         }
      });
   } else {
      response.writeHead(404, {'Content-Type' : 'text/html'});
      response.end('404 Page Not Found');
   }
});

server.listen(8080, () => console.log("Server is running..."));