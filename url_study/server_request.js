const http = require('http')
const url = require('url')

const server = http.createServer((request, response) =>{
   console.log(request.url);

   const parseUrl = url.parse(request.url);
   const resource = parseUrl.pathname;

   console.log('resource path=%s', resource);

   if(resource === '/address'){
       responseDisplay('서울특별시 강남구 논현1동 111');
   } else if(resource === '/phone'){
       responseDisplay('02-3545-1237');
   } else if(resource === '/name'){
       responseDisplay('Hong Gil Dong');
   } else {
       responseDisplay('404 Page Not Found');
   }

   function responseDisplay(content) {
       response.writeHead(200, {'Content-Type' : 'text/html; charset=utf-8'});
       response.end(content);
   }
});

server.listen(8080, () => console.log('Server is runnig...'));