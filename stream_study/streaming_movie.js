const http = require('http');
const url = require('url');
const fs = require('fs');

const server = http.createServer((request, response) => {
   const parseUrl = url.parse(request.url);
   const resource = parseUrl.pathname;
   console.log('resource='+resource);

   const resourcePath = '.' + resource;
   console.log('resourcePath=' + resourcePath);

   if(resource.indexOf('/html/') === 0){
       fs.readFile(resourcePath, 'utf-8', (err, data) => {
           if(err){
               response.writeHead(500, {'Content-Type' : 'text/html'});
               response.end('500 Internal Server' + err);
           } else {
               response.writeHead(200, {'Content-Type' : 'text/html'});
               response.end(data);
           }
       });
   } else if(response.indexOf('/movie') === 0){
       const stream = fs.createReadStream(resourcePath);

       stream.on('data', data => response.write(data));
       stream.on('end', () => {
           console.log('end streaming');
           response.end();
       });
       stream.on('error', err => {
           console.log(err);
           response.end('500 Internal Server' + err);
       });
   } else {
       response.writeHead(404, {'Content-Type' : 'text/html'});
       response.end('404 Page Not Found');
   }
});

server.listen(8080, ()=> console.log('Server is running...'));