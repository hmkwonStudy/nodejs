const http = require('http');
const url = require('url');
const fs = require('fs');

const server = http.createServer((request, response) =>{
   const parseUrl = url.parse(request.url);
   const resource = parseUrl.pathname;

   if(resource === '/hello'){
       fs.readFile('hello.html', 'utf-8', (err, data) =>{
           if(err){
               response.writeHead(500, {'Content-Type' : 'text/html'});
               response.end('500 Internal Server Error : ' + err);
           } else {
               response.writeHead(200, {'Content-Type' : 'text/html'});
               response.end(data);
           }
       });
   } else {
       response.writeHead(404, {'Content-Type' : 'text/html'});
       response.end('404 Page Not Found');
   }
});

server.listen(8080, () => console.log('Server is runnig...'));