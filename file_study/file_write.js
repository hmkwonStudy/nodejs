const fs = require('fs');

const data = 'My first data...\r\nhello there!';

fs.writeFile('file01_async.txt', data, 'utf-8', err => {
    err ? console.log(err) : console.log('01 write done');
});

try {
    fs.writeFileSync('file02_sync.txt', data, 'utf-8');
    console.log('02 write done!');
} catch (err) {
    console.log(err);
}