const fs = require('fs');

fs.readFile('../module/home.js'
            , 'utf-8'
            , (err, data) =>{
                    console.log('01 readAsync: \n%s', data);
            });

const data = fs.readFileSync('../module/home.js', 'utf-8');

console.log('02 readSync: \n%s', data);