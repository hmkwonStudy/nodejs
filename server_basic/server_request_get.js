const http = require('http');
const url = require('url');
const querystring = require('querystring');


//http://localhost:8080/?var1=newData&var2=153&var3=testdata2017
const server = http.createServer((request, response) => {
    const parsedUrl = url.parse(request.url);
    const parsedQuery = querystring.parse(parsedUrl.query, '&', '=');
    console.log(parsedQuery);
    response.writeHead(200, {'Content-Type' : 'text/html'});
    response.end(parsedQuery.var1);
});

server.listen(8080, () => {
    console.log('Server is runnig...');
});