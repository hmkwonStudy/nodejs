// require => node.js에서 외부 모듈을 가져오는 데 사용
let http = require('http');

let server = http.createServer((request, response) => {
    response.writeHead(200, {'Content-Type' : 'text/html'});
    response.end('Hello node.js');
});

server.listen(8080, () => {
    console.log('Server is runnig...');
});