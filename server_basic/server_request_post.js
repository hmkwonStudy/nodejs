const http = require('http');
const querystring = require('querystring');

const server = http.createServer((request, response) =>{
   let postdata = '';
   request.on('data', data => postdata = postdata + data);
   request.on('end', () =>{
       const parsedQuery = querystring.parse(postdata);
       response.writeHead(200, {'Content-Type' : 'text/html'});

       console.log(parsedQuery);
       response.end();
   });
});
server.listen(8080, () => console.log("Server is running..."));